#!/usr/bin/env node
const Extr = require('./html_extracter');
const fs = require('fs');
let extr = new Extr
let file = process.argv[2];
if (!file) console.err('Please specify an HTML file path (eg. test/target/test.html)')
let htmlStr = fs.readFileSync(file, "utf8")
let parsedStr = extr.parse(htmlStr)
if (!parsedStr) return console.err('Replace error')
fs.writeFileSync(file+'.replaced', parsedStr,{ flag : 'w' })
console.log('Replace completed')
fs.writeFileSync(file+'.json', JSON.stringify(extr.json, null, 2),{ flag : 'w' })
console.log('JSON generated')

