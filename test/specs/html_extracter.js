const expect = require('chai').expect
const Extr = require('../../html_extracter');

describe('Html extracter',()=>{
	let extr = null;
	
	beforeEach(()=>{
			extr = new Extr()
	})
	
	it('should extract plain text',()=>{
		expect(extr.parse('Hi <"userA132">, I\'m doing bad things<><>'))
		.to.equal('{{text.hiUserA132IMDoing}}')
	})
	it('should extract basic valid html',()=>{
		expect(extr.parse('<div class="col-lg-9"><div class="crumb"><a href="m4#videogrid">< Back</a></div><h1>SwiftCam M4</h1><div class="description">Axis stabilizing technology for mobile phone</div><div class="gallery-wrap"><m4-simple-gallery slides="m4Vm.currentBuyable.slides"></m4-simple-gallery></div></div>'))
			.to.equal('<div class="col-lg-9"><div class="crumb"><a href="m4#videogrid">{{text.back}}</a></div><h1>{{text.swiftCamM4}}</h1><div class="description">{{text.axisStabilizingTechnologyForMobile}}</div><div class="gallery-wrap"><m4-simple-gallery slides="m4Vm.currentBuyable.slides"></m4-simple-gallery></div></div>')
	})
	it('should only transform non {{someText}} in a leaf node while preserving heading / trailing space',()=>{
		expect(extr.parse('<div>{{someText}} hello my{{someText}}world {{someText}}</div>'))
			.to.equal('<div>{{someText}} {{text.helloMy}}{{someText}}{{text.world}} {{someText}}</div>')
	})
	it('should ignore quote in bracket',()=>{
		expect(extr.parse("<h2>{{m4Vm.currentBuyable.currency | translateCurrency}} {{m4Vm.currentBuyable.price | currency:'':2}}</h2>"))
			.to.equal("<h2>{{m4Vm.currentBuyable.currency | translateCurrency}} {{m4Vm.currentBuyable.price | currency:'':2}}</h2>")
	})
	it('should generate a json with camcelCase as key and extracted text as value',()=>{
		extr.parse('<div class="sub-menu"><nav class="navbar navbar-default" ><div class="container"><ul class="nav navbar-nav navbar-right"><li><a href="m4#m4spec">Specification</a></li><li><a href="m4#m4modes">Modes</a></li><li><a href="m4#m4tutorial">Tutorial</a></li><li><a href="m4#m4buy" class="buy-now-button" type="button" name="button">Buy Now</a></li></ul></div></nav></div>')
		expect(extr.json).to.deep.equal({
			specification:'Specification',
			modes:'Modes',
			tutorial:'Tutorial',
			buyNow:'Buy Now'
		})
	})
	it('should able to extract duplicated text',()=>{
		expect(extr.parse('<p>Hi <"userA132">, I\'m doing bad things<><></p><p>Hi <"userA132">, I\'m doing bad things<><></p><p>Buy Now</p>'))
			.to.equal('<p>{{text.hiUserA132IMDoing}}</p><p>{{text.hiUserA132IMDoing}}</p><p>{{text.buyNow}}</p>')
		expect(extr.json).to.deep.equal({hiUserA132IMDoing:'Hi <"userA132">, I\'m doing bad things<><>',buyNow:'Buy Now'})
	})
	it('should not have leading number', ()=>{
		expect(extr.parse('<p>360 panorama</p>'))
			.to.equal('<p>{{text._360Panorama}}</p>')
	})

})