# replace_html_placeholder
I10n tools for extracting HTML text `<span>Hello World</span>` to `<span>{{text.helloWorld}}<span>` and write result to a json file

# Default Behaviour

- text transform: camel-case (Hello World -> helloWorld)
- long text: will only camal-case the first four character
- prefix: text.[YOUR_TRANSFORMED_TEXT] (todo: configurable)

# How to run locally

```
npm install -g .
replace_html_placeholder my-page.html
```

# How to run without cloning source
```
npm install -g roycwc/replace_html_placeholder
replace_html_placeholder path/to/my-page.html
```

# Generated result
- `path/to/my-page.html.replaced` - replaced html
- `path/to/my-page.html.json` - extracted key/value json

# How to test

```
npm test
```

MIT