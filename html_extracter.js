

class HtmlExtracter{
	constructor(){
		this.cheerio = require('cheerio')
		this.camelCase = require('camelcase');
		this.mustache = require('mustache');
		this.mustache.clearCache()
		this.json={}
	}
	parse(html){
		let $ = this.cheerio.load('<div>'+html+'</div>')
		$('*').filter((index, elm)=>{
			return $(elm).children().length == 0 && $(elm).text()
		}).map((index, elm)=>{
			let transform = $(elm).text()
			let parsed = this.mustache.parse(transform)
			let parsedTransformed = parsed.map((item)=>{
				if (item[0]=='text'){
					let innerText = item[1]
					if (innerText.indexOf('{{')==0 && innerText.indexOf('}}')==innerText.length-2){
					}else{
						let headingSpaceCount = 0, trailingSpaceCount = 0
						try{
							headingSpaceCount = innerText.match(/^\s/)[0].length
						}catch(e){}
						try{
							trailingSpaceCount = innerText.match(/\s*$/)[0].length
							if (!innerText.match(/\S+/g)) trailingSpaceCount = 0
						}catch(e){}
						let originalText = innerText
						innerText = innerText.replace(/[\W]/g, ' ') // replace non alphabat to space
						innerText = innerText.split(' ').filter(it=>it).slice(0,5).join(' ') // remove repetitve space and slice 5 pcs and concat with space
						innerText = this.camelCase(innerText)
						innerText = innerText.match(/^[0-9]/) ? '_'+innerText : innerText
						if (innerText) this.json[innerText] = originalText
						item[1] = ' '.repeat(headingSpaceCount)
												+ (innerText ? '{{text.'+innerText+'}}' : '')
												+ ' '.repeat(trailingSpaceCount) 
					}
				}
				return item
			}).map((row)=>{
				if (row[0] == 'name') return '{{'+row[1]+'}}' // those originally have curly braces
				return row[1]
			}).join('')
			return $(elm).text(parsedTransformed.trim())
		})
		return $('div').html().replace(/\&apos\;/g,"'")
	}
}

module.exports = HtmlExtracter